/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.8 $ $Author: srikrish $ $Date: 2004/09/07 00:06:01 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.types;

import gov.cca.Type;
import gov.cca.TypeMap;

import xcat.exceptions.XCATTypeMismatchException;

import java.util.Hashtable;

import soaprmi.util.logging.Logger;

/**
 *  This class implements the CCA TypeMap interface
 * 
 *  A CCA map.  Maps a string key to a particular value. Types are
 *  strictly enforced.  For example, values places into the map
 *  using putInt can be retrieved only using getInt.  Calls to
 *  getLong, getString, getIntArray and other get methods will
 *  fail (i.e. return the default value). 
 */
public class TypeMapImpl implements TypeMap {

  private static Logger logger = Logger.getLogger();

  private Hashtable map;

  /**
   * Constructor
   */
  public TypeMapImpl() {
    logger.finest("called");
    map = new Hashtable();
  }

  /**
   * Constructor
   * @param map_ Hashmap to initialize with
   */
  private TypeMapImpl(Hashtable map_) {
    logger.finest("called with: " + map_);
    map = map_;
  }

  /**
   * Create an exact copy of this Map 
   */
  public gov.cca.TypeMap cloneTypeMap() {
    logger.finest("called");
    return new TypeMapImpl((Hashtable) map.clone());
  }

  /**
   * Create a new Map with no key/value associations. 
   */
  public gov.cca.TypeMap cloneEmpty() {
    logger.finest("called");
    return new TypeMapImpl();
  }

  /**
   * Method:  getInt[]
   */
  public int getInt(java.lang.String key,
		    int dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof Integer)
	return ((Integer)value).intValue();
      else
	throw new XCATTypeMismatchException(Type.Int,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

  /**
   * Method:  getLong[]
   */
  public long getLong(java.lang.String key,
		      long dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof Long)
	return ((Long)value).longValue();
      else
	throw new XCATTypeMismatchException(Type.Long,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

  /**
   * Method:  getFloat[]
   */
  public float getFloat(java.lang.String key,
			float dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof Float)
	return ((Float)value).floatValue();
      else
	throw new XCATTypeMismatchException(Type.Float,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

  /**
   * Method:  getDouble[]
   */
  public double getDouble(java.lang.String key,
			  double dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof Double)
	return ((Double)value).doubleValue();
      else
	throw new XCATTypeMismatchException(Type.Double,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

//    /**
//     * Method:  getFcomplex[]
//     */
//    public SIDL.FloatComplex getFcomplex(java.lang.String key,
//  				       SIDL.FloatComplex dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.FloatComplex)
//  	return (SIDL.FloatComplex) value;
//        else
//  	throw new XCATTypeMismatchException(Type.Fcomplex,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getDcomplex[]
//     */
//    public SIDL.DoubleComplex getDcomplex(java.lang.String key,
//  					SIDL.DoubleComplex dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.DoubleComplex)
//  	return (SIDL.DoubleComplex) value;
//        else
//  	throw new XCATTypeMismatchException(Type.Dcomplex,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

  /**
   * Method:  getString[]
   */
  public java.lang.String getString(java.lang.String key,
				    java.lang.String dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof String)
	return (String) value;
      else
	throw new XCATTypeMismatchException(Type.String,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

  /**
   * Method:  getBool[]
   */
  public boolean getBool(java.lang.String key,
			 boolean dflt) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      Object value = map.get(key);
      if (value instanceof Boolean)
	return ((Boolean)value).booleanValue();
      else
	throw new XCATTypeMismatchException(Type.Bool,
					    TypeUtil.getType(value));
    } else {
      return dflt;
    }
  }

//    /**
//     * Method:  getIntArray[]
//     */
//    public SIDL.Integer.Array1 getIntArray(java.lang.String key,
//  					 SIDL.Integer.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.Integer.Array1)
//  	return (SIDL.Integer.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.IntArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getLongArray[]
//     */
//    public SIDL.Long.Array1 getLongArray(java.lang.String key,
//  				       SIDL.Long.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.Long.Array1)
//  	return (SIDL.Long.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.LongArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getFloatArray[]
//     */
//    public SIDL.Float.Array1 getFloatArray(java.lang.String key,
//  					 SIDL.Float.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.Float.Array1)
//  	return (SIDL.Float.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.FloatArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getDoubleArray[]
//     */
//    public SIDL.Double.Array1 getDoubleArray(java.lang.String key,
//  					   SIDL.Double.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.Double.Array1)
//  	return (SIDL.Double.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.DoubleArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getFcomplexArray[]
//     */
//    public SIDL.FloatComplex.Array1 getFcomplexArray(java.lang.String key,
//  						   SIDL.FloatComplex.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.FloatComplex.Array1)
//  	return (SIDL.FloatComplex.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.FcomplexArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getDcomplexArray[]
//     */
//    public SIDL.DoubleComplex.Array1 getDcomplexArray(java.lang.String key,
//  						    SIDL.DoubleComplex.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.DoubleComplex.Array1)
//  	return (SIDL.DoubleComplex.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.DcomplexArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getStringArray[]
//     */
//    public SIDL.String.Array1 getStringArray(java.lang.String key,
//  					   SIDL.String.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.String.Array1)
//  	return (SIDL.String.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.StringArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

//    /**
//     * Method:  getBoolArray[]
//     */
//    public SIDL.Boolean.Array1 getBoolArray(java.lang.String key,
//  					  SIDL.Boolean.Array1 dflt) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value = map.get(key);
//        if (value instanceof SIDL.Boolean.Array1)
//  	return (SIDL.Boolean.Array1) value;
//        else
//  	throw new XCATTypeMismatchException(Type.BoolArray,
//  					    TypeUtil.getType(value));
//      } else {
//        return dflt;
//      }
//    }

  /**
   * Assign a key and value. Any value previously assigned
   * to the same key will be overwritten so long as it
   * is of the same type. If types conflict, an exception occurs.
   */
  public void putInt(java.lang.String key,
		     int value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof Integer)
	map.put(key, new Integer(value));
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.Int);
    } else
      map.put(key, new Integer(value));
  }

  /**
   * Method:  putLong[]
   */
  public void putLong(java.lang.String key,
		      long value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof Long)
	map.put(key, new Long(value));
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.Long);
    } else
      map.put(key, new Long(value));
  }

  /**
   * Method:  putFloat[]
   */
  public void putFloat(java.lang.String key,
		       float value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof Float)
	map.put(key, new Float(value));
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.Float);
    } else
      map.put(key, new Float(value));
  }

  /**
   * Method:  putDouble[]
   */
  public void putDouble(java.lang.String key,
			double value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof Double)
	map.put(key, new Double(value));
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.Double);
    } else
      map.put(key, new Double(value));
  }

//    /**
//     * Method:  putFcomplex[]
//     */
//    public void putFcomplex(java.lang.String key,
//  			  SIDL.FloatComplex value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.FloatComplex)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.Fcomplex);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putDcomplex[]
//     */
//    public void putDcomplex(java.lang.String key,
//  			  SIDL.DoubleComplex value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.DoubleComplex)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.Dcomplex);
//      } else
//        map.put(key, value);
//    }

  /**
   * Method:  putString[]
   */
  public void putString(java.lang.String key,
			java.lang.String value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof String)
	map.put(key, value);
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.String);
    } else
      map.put(key, value);
  }

  /**
   * Method:  putBool[]
   */
  public void putBool(java.lang.String key,
		      boolean value) 
    throws gov.cca.TypeMismatchException {
    logger.finest("called with key: " + key + " and value: " + value);
    if (map.containsKey(key)) {
      Object value_ = map.get(key);
      if (value_ instanceof Boolean)
	map.put(key, new Boolean(value));
      else
	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
					    Type.Bool);
    } else
      map.put(key, new Boolean(value));
  }

//    /**
//     * Method:  putIntArray[]
//     */
//    public void putIntArray(java.lang.String key,
//  			  SIDL.Integer.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.Integer.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.IntArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putLongArray[]
//     */
//    public void putLongArray(java.lang.String key,
//  			   SIDL.Long.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.Long.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.LongArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putFloatArray[]
//     */
//    public void putFloatArray(java.lang.String key,
//  			    SIDL.Float.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.Float.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.FloatArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putDoubleArray[]
//     */
//    public void putDoubleArray(java.lang.String key,
//  			     SIDL.Double.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.Double.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.DoubleArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putFcomplexArray[]
//     */
//    public void putFcomplexArray(java.lang.String key,
//  			       SIDL.FloatComplex.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.FloatComplex.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.FcomplexArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putDcomplexArray[]
//     */
//    public void putDcomplexArray(java.lang.String key,
//  			       SIDL.DoubleComplex.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.DoubleComplex.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.DcomplexArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putStringArray[]
//     */
//    public void putStringArray(java.lang.String key,
//  			     SIDL.String.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.String.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.StringArray);
//      } else
//        map.put(key, value);
//    }

//    /**
//     * Method:  putBoolArray[]
//     */
//    public void putBoolArray(java.lang.String key,
//  			   SIDL.Boolean.Array1 value) 
//      throws gov.cca.TypeMismatchException {
//      if (map.containsKey(key)) {
//        Object value_ = map.get(key);
//        if (value_ instanceof SIDL.Boolean.Array1)
//  	map.put(key, value);
//        else
//  	throw new XCATTypeMismatchException(TypeUtil.getType(value_),
//  					    Type.BoolArray);
//      } else
//        map.put(key, value);
//    }

  /**
   * Make the key and associated value disappear from the object. 
   */
  public void remove(java.lang.String key) {
    logger.finest("called with key: " + key);
    map.remove(key);
  }

  /**
   *  Get all the names associated with the TypeMap
   *  without exposing the data implementation details.  The keys
   *  will be returned in an arbitrary order.
   *
   */
  public String[] getAllKeys() {
    Object[] objArray = map.keySet().toArray();
    String[] keys = new String[objArray.length];
    for (int i = 0; i < objArray.length; i++)
      keys[i] = (String) objArray[i];
    return keys;
  }

  /**
   * Return true if the key exists in this map 
   */
  public boolean hasKey(java.lang.String key) {
    logger.finest("called with key: " + key);
    return map.containsKey(key);
  }

  /**
   * Return the type of the value associated with this key 
   */
  public int typeOf(java.lang.String key) {
    logger.finest("called with key: " + key);
    Object value = map.get(key);
    return TypeUtil.getType(value);
  }

  /**
   * Return the stringified form of the object associated with this key
   */
  public String getAsString(java.lang.String key) {
    logger.finest("called with key: " + key);
    if (map.containsKey(key)) {
      return map.get(key).toString();
    } else
      return null;
  }
}
