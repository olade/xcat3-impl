/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.10 $ $Author: srikrish $ $Date: 2004/09/07 00:06:01 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.types;

import java.util.Hashtable;
import java.io.StringReader;
import java.io.StringWriter;

import gov.cca.Type;
import gov.cca.TypeMap;

import xcat.exceptions.NonstandardException;

import org.gjt.xpp.XmlPullNode;
import org.gjt.xpp.XmlPullParser;
import org.gjt.xpp.XmlPullParserFactory;

import soaprmi.util.logging.Logger;

/**
 * A utility class for Types
 */
public class TypeUtil {

  private static Logger logger = Logger.getLogger();
  private static Hashtable typeMap = null;

  // Initialize the typeMap Hashtable object
  static {
    if (typeMap == null) {
      typeMap = new Hashtable();
      typeMap.put("None", new Integer(Type.None));
      typeMap.put(Integer.class.getName(), new Integer(Type.Int));
      typeMap.put(Long.class.getName(), new Integer(Type.Long));
      typeMap.put(Float.class.getName(), new Integer(Type.Float));
      typeMap.put(Double.class.getName(), new Integer(Type.Double));
      typeMap.put(String.class.getName(), new Integer(Type.String));
      typeMap.put(Boolean.class.getName(), new Integer(Type.Bool));

      // The following doesn't work as the JVM can't find a native implementation
      // of a method declared native
      //        typeMap.put(SIDL.FloatComplex.class.getName(), new Integer(5));
      //        typeMap.put(SIDL.DoubleComplex.class.getName(), new Integer(6));
      //        typeMap.put(SIDL.Integer.Array1.class.getName(), new Integer(9));
      //        typeMap.put(SIDL.Long.Array1.class.getName(), new Integer(10));
      //        typeMap.put(SIDL.Float.Array1.class.getName(), new Integer(11));
      //        typeMap.put(SIDL.Double.Array1.class.getName(), new Integer(12));
      //        typeMap.put(SIDL.FloatComplex.Array1.class.getName(), new Integer(13));
      //        typeMap.put(SIDL.DoubleComplex.Array1.class.getName(), new Integer(14));
      //        typeMap.put(SIDL.String.Array1.class.getName(), new Integer(15));
      //        typeMap.put(SIDL.Boolean.Array1.class.getName(), new Integer(16));
    }
  }

  /**
   * Returns the Type specified by gov.cca.Type for the particular
   * object
   * @param typeObject the type whose CCA Type is required
   */
  public static int getType(Object typeObject) {
    
    logger.finest("called for Object of type: " + typeObject.getClass());
    String key = typeObject.getClass().getName();

    if (typeMap.containsKey(key))
      return ((Integer) typeMap.get(key)).intValue();
    else
      return Type.None;
  }

  /**
   * Returns the stringified form of the type (FQN of Java class)
   * @param type the type number defined by CCA
   */
  public static String toString(int type) {
    switch (type) { 
    case Type.Int: return Integer.class.getName();
    case Type.Long: return Long.class.getName();
    case Type.Float: return Float.class.getName();
    case Type.Double: return Double.class.getName();
    case Type.String: return String.class.getName();
    case Type.Bool: return Boolean.class.getName();
    default: return Object.class.getName();
    }
  }

  /**
   * Puts in an object of an acceptable type into the TypeMap
   * @param tMap the typeMap object to insert into
   * @param key the key to use for the object
   * @param value the stringified form of the value object
   * @param type the FQN of the type of the value
   */
  public static void put(TypeMap tMap,
			 String key,
			 String value,
			 String type) 
    throws gov.cca.CCAException {
    if (type.equals(Integer.class.getName())) {
      int i = Integer.parseInt(value);
      tMap.putInt(key, i);
      return;
    } else if (type.equals(Long.class.getName())) {
      long l = Long.parseLong(value);
      tMap.putLong(key, l);
      return;
    } else if (type.equals(Float.class.getName())) {
      float f = Float.parseFloat(value);
      tMap.putFloat(key, f);
      return;
    } else if (type.equals(Double.class.getName())) {
      double d = Double.parseDouble(value);
      tMap.putDouble(key, d);
      return;
    } else if (type.equals(String.class.getName())) {
      tMap.putString(key, value);
      return;
    } else if (type.equals(Boolean.class.getName())) {
      boolean b = Boolean.valueOf(value).booleanValue();
      tMap.putBool(key, b);
    } else 
      throw new NonstandardException("Can not handle type: " + type);
  }
  
  /**
   * Return the XML-ised form of the TypeMap
   * @param tMap the typeMap object to be xml-ised
   */
  public static String toXML(TypeMap tMap) {
    logger.finest("called");
    StringBuffer sb = new StringBuffer();
    sb.append("<properties>");
    String[] mapKeys = tMap.getAllKeys();
    for (int j = 0; j < mapKeys.length; j++) {
      sb.append("<propertiesEntry>");
      sb.append("<key>" +
		mapKeys[j] +
		"</key>");
      sb.append("<type>" +
		TypeUtil.toString(tMap.typeOf(mapKeys[j])) +
		"</type>");
      sb.append("<value>" +
		tMap.getAsString(mapKeys[j]) +
		"</value>");
      sb.append("</propertiesEntry>");
    }
    sb.append("</properties>");
    return sb.toString();
  }

  /**
   * Load the values from XML-ised form
   * @param properties the XML form of the TypeMap generated by toXML()
   * @param tMap the typeMap object to load the values into
   */
  public static void fromXML(String properties, TypeMap tMap) 
    throws gov.cca.CCAException {
    try {
      // convert the properties object to an XmlPullNode
      XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
      XmlPullParser pp = factory.newPullParser();
      pp.setInput(new StringReader(properties));
      pp.next();
      XmlPullNode propertiesNode = factory.newPullNode(pp); 
      while (true) {
	// read the next <propertiesEntry/>
	XmlPullNode entryNode = (XmlPullNode) propertiesNode.readNextChild();
	if (entryNode == null)
	  break;
	
	String key = null;
	String value = null;
	String type = null;
	// process the entry
	while (true) {
	  XmlPullNode nextChild = (XmlPullNode) entryNode.readNextChild();
	  if (nextChild == null)
	    break;
	  
	  // get the key, value, and type
	  if (nextChild.getLocalName().equals("key")) {
	    key = (String) nextChild.readNextChild();
	  } else if (nextChild.getLocalName().equals("value")) {
	    value = (String) nextChild.readNextChild();
	  } else { // (nextChild.getLocalName().equals("type"))
	    type = (String) nextChild.readNextChild();
	  }
	}
	// add the entry to the typeMap
	put(tMap, key, value, type);
      } 
    } catch (Exception e) {
      logger.severe("Exception while reading TypeMap from XML", e);
      throw new NonstandardException("Exception while reading TypeMap from XML", 
				     e);
    }
  }
}
