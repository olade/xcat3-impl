/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.2 $ $Author: srikrish $ $Date: 2004/09/07 00:06:02 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.util;

import java.util.Hashtable;
import intf.ports.XCATPort;
import soaprmi.util.logging.Logger;
import xcat.exceptions.NonstandardException;

/**
 * LocalServiceRegistry is a registry of local services for providing
 * optimization for co-located services.
 */
public class LocalServiceRegistry {

  private static Logger logger = Logger.getLogger();
  private static Hashtable serviceMap = new Hashtable();

  /**
   * Bind a service handle to the implementation object
   * @param serviceHandle the GSH for the service
   * @param serviceImpl the implementation of the XCATPort that 
   *                    represents this service
   * @exception NonstandardException if the serviceHandle is already in
   *            the registry
   */
  public static void bind(String serviceHandle,
			  XCATPort serviceImpl) 
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + serviceHandle);
    if (serviceMap.containsKey(serviceHandle))
      throw new NonstandardException("Service handle : " + serviceHandle +
				     " already in the registry");
    serviceMap.put(serviceHandle, serviceImpl);
  }

  /**
   * Bind a service handle to the implementation object (even if a prior
   * binding exists)
   * @param serviceHandle the GSH for the service
   * @param serviceImpl the implementation of the XCATPort that 
   *                    represents this service
   */
  public static void rebind(String serviceHandle,
			    XCATPort serviceImpl) 
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + serviceHandle);
    serviceMap.put(serviceHandle, serviceImpl);
  }

  /**
   * Unbind a service handle from the local registry
   * @param serviceHandle the GSH for the service
   * @exception NonstandardException if the handle is not in the registry
   */
  public static void unbind(String serviceHandle)
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + serviceHandle);
    if (!serviceMap.containsKey(serviceHandle))
      throw new NonstandardException("Service handle: " + serviceHandle +
				     " not in registry");
    serviceMap.remove(serviceHandle);
  }

  /**
   * Checks to see if this service handle exists in the registry
   * @param serviceHandle the GSH for the service
   */
  public static boolean containsService(String serviceHandle)
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + serviceHandle);
    return serviceMap.containsKey(serviceHandle);
  }

  /**
   * Returns the XCATPort implementation that corresponds to this serviceHandle
   * @param serviceHandle the GSH for the service
   * @exception NonstandardException if the handle is not in the registry
   */
  public static XCATPort getServiceImpl(String serviceHandle)
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + serviceHandle);
    if (!serviceMap.containsKey(serviceHandle))
      throw new NonstandardException("Service handle: " + serviceHandle +
				     " not in registry");
    return (XCATPort) serviceMap.get(serviceHandle);
  }
}
