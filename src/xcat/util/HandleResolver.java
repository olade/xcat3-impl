/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.9 $ $Author: srikrish $ $Date: 2004/06/09 04:34:29 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.util;

import intf.ports.XCATPort;

import xcat.exceptions.NonstandardException;

import soaprmi.util.logging.Logger;
import soaprmi.soaprpc.SoapServices;
import soaprmi.server.RemoteRef;
import soaprmi.ogsi.util.ClientUtil;
import soaprmi.ogsi.base_types.LocatorType;
import soaprmi.ogsi.base_types.ReferenceType;
import soaprmi.ogsi.util.XmlUtil;
import soaprmi.wsdl.WSDLUtil;

import org.gjt.xpp.XmlNode;
import org.gjt.xpp.XmlPullParser;
import org.gjt.xpp.XmlPullParserFactory;
import soaprmi.util.logging.Logger;

import java.util.Random;
import java.io.StringReader;


/**
 * HandleResolver class to provide utility methods for resolving
 * GSHs to valid GSRs
 */
public class HandleResolver {

  private static Logger logger = Logger.getLogger();
  private static Random random = new Random();

  // The real Handle Resolver interface that is used
  private static String handleResolverURL = null;
  
  /**
   * Sets the Handle Resolver that is used locally
   * @param handleResolverURL_ the URL of the GSX Handle Resolver
   */
  public static void setHandleResolverURL(String handleResolverURL_) {
    logger.finest("called with URL: " + handleResolverURL_);
    handleResolverURL = handleResolverURL_;
  }
  
  /**
   * Get the Handler Resolver URL that is used
   */
  public static String getHandleResolverURL() {
    logger.finest("called");
    return handleResolverURL;
  }
  
  /**
   * A utility method that uses the URL for the HandleResolver to create
   * a unique GSH
   * @param instanceName the name of the service (need not be unique)
   */
  public static String createGSH(String instanceName) 
    throws gov.cca.CCAException {
    logger.finest("called with instanceName: " + instanceName);
    if (handleResolverURL == null) {
      logger.severe("Can't create a GSH since URL for HandleResolver is Null");
      throw new NonstandardException("Can't create a GSH since URL for HandleResolver is Null");
    }
    return handleResolverURL + "/" + instanceName + "/" + Math.abs(random.nextInt());
  }

  /**
   * Adds a reference into the Handle Resolver
   * @param handle the GSH to be registered
   * @param reference the XCATPort that serves as the java binding of the GSR
   */
  public static void addReference(String handle, XCATPort reference) 
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + handle);

    // create an entry in the local registry
    LocalServiceRegistry.rebind(handle, reference);

    // make sure the handleResolver exists
    if (handleResolverURL == null) {
      logger.warning("Unable to locate a Handle Resolver implementation: " + 
		     " only able to add a reference to the Local Registry");
      return;
    }

    // create the stringified WSDL
    // String wsdlString = null;
    // create the stringified XSOAP reference
    String xmlString = null;
    try {
      // wsdlString = WSDLUtil.convertRefToWSDL((RemoteRef) reference,
      //  				        reference.getClass().getName());
      xmlString = SoapServices.getDefault().getStartpointXml(reference);
    } catch (Exception e) {
      // logger.severe("Can not convert XSOAP remote reference to WSDL", e);
      // throw new NonstandardException("Can not convert XSOAP remote reference to WSDL", e);
      logger.severe("Can not convert XSOAP remote reference to String", e);
      throw new NonstandardException("Can not convert XSOAP remote reference to String", e);
    }
    
    // create the XmlBean ReferenceType
    // ReferenceType wsdlReference = null;
    ReferenceType xmlReference = null;
    try {
      // wsdlReference = (ReferenceType) ReferenceType.Factory.parse(wsdlString);
      xmlReference = (ReferenceType) ReferenceType.Factory.parse(xmlString);
    } catch (Exception e) {
      // logger.severe("Can not convert stringified WSDL to XmlBean ReferenceType", e);
      // throw new NonstandardException("Can not convert stringified WSDL to XmlBean ReferenceType", 
      //                                e);
      logger.severe("Can not convert stringified reference to XmlBean ReferenceType", e);
      throw new NonstandardException("Can't convert stringified reference to XmlBean ReferenceType", 
				     e);
    }

    // add a reference in the real Handle Resolver
    try {
      // ClientUtil.setLocator(handleResolverURL,
      //		       handle,
      //		       wsdlReference);
      ClientUtil.setLocator(handleResolverURL,
			    handle,
			    xmlReference);
    } catch (Exception e) {
      logger.severe("Can not register reference with Handle Resolver", e);
      throw new NonstandardException("Can not register reference with Handle Resolver", e);
    }
  }

  /**
   * Resolves a handle to a reference, if possible.
   * @param handle the GSH to be resolved to a GSR
   * @param intfClassName the fully qualified class name for the remote interface
   */
  public static XCATPort resolveHandle(String handle, String intfClassName) 
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + handle);

    long start = System.currentTimeMillis();

    // Initialize the return value
    XCATPort reference = null;
    
    // check to see if this service exists in the same address space
    if (LocalServiceRegistry.containsService(handle))
      reference = (XCATPort) LocalServiceRegistry.getServiceImpl(handle);
    else {
      // make sure the handleResolver exists
      if (handleResolverURL == null)
	throw new NonstandardException("Unable to locate a Handle Resolver implementation");

      // Use the real HandleResolver to get a reference
      LocatorType portLocator = null; 
      
      try {
	portLocator = ClientUtil.findByHandle(handleResolverURL,
					      handle);
      } catch (Exception e) {
	logger.severe("Exception while trying to find a Reference via Handle", 
		      e);
	throw new NonstandardException("Exception while trying to find a Reference via Handle", 
				       e);
      }

      ReferenceType[] portRef = portLocator.getReferenceArray();
      
      if (portRef.length > 0) {
	String portGSR = XmlUtil.getChildOf(portRef[0]);
	if (portGSR != null) {
	  try {
	    // reference = (XCATPort) WSDLUtil.convertWSDLToRef(portGSR);
	    reference = 
	      (XCATPort) SoapServices.getDefault().createStartpointFromXml(portGSR);
	  } catch (Exception e) {
	    // logger.severe("Can not convert WSDL to XSOAP Reference", e);
	    // throw new NonstandardException("Can not convert WSDL to XSOAP Reference", 
	    //          		      e);
	    logger.severe("Can not convert stringified reference to XCATPort", e);
	    throw new NonstandardException("Can not convert stringified reference to XCATPort",
					   e);
	  }
	} else {
	  logger.severe("Reference for " + handle + 
			" found to be null");
	  throw new NonstandardException("Reference for " + handle + 
					 " found to be null");
	}
      } else {
	logger.severe("No reference found for " + handle + 
		      " in Resolver: " + handleResolverURL);
	throw new NonstandardException("No reference found for " + handle + 
				       " in Resolver: " + handleResolverURL);
      }
    }
    
    // check if the returned object has the expected interface
    logger.finest("Returned class: " + reference.getClass().getName());
    try {
      if (!(Class.forName(intfClassName).isAssignableFrom(reference.getClass())))
	throw new NonstandardException("Returned reference is not an instance of " +
				       intfClassName);
    } catch (ClassNotFoundException cnfe) {
      logger.severe("Can't verify if returned class conforms to specified interface",
		    cnfe);
      throw new NonstandardException("Can't verify if returned class conforms " + 
				     "to specified interface", cnfe);
    }

    long end = System.currentTimeMillis();
    logger.finest("Time for handle resolution for " + intfClassName + 
		  ": " + (end - start));
    return reference;
  }
}
