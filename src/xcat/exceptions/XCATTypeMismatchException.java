/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.3 $ $Author: srikrish $ $Date: 2004/01/02 01:10:39 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.exceptions;

import gov.cca.TypeMismatchException;
import java.io.StringWriter;
import java.io.PrintWriter;

/**
 * Implementation of the TypeMismatchException abstract class
 */
public class XCATTypeMismatchException extends TypeMismatchException {

  private int requestedType;
  private int actualType;
  private String note;

  /**
   * Constructor
   * @param requestedType_ the enumerated value Type requested
   * @param actualType_ the actual enumerated value Type received
   */
  public XCATTypeMismatchException(int requestedType_,
				   int actualType_) {
    this(requestedType_, actualType_, null);
  }

  /**
   * Constructor
   * @param requestedType_ the enumerated value Type requested
   * @param actualType_ the actual enumerated value Type received
   * @param note_ the actual message for the exception
   */
  public XCATTypeMismatchException(int requestedType_,
				   int actualType_,
				   String note_) {
    note = note_;
    requestedType = requestedType_;
    actualType = actualType_;
  }

  /**
   * @return the enumerated value Type sought 
   */
  public int getRequestedType() {
    return requestedType;
  }

  /**
   * @return the enumerated value Type sought 
   */
  public int getActualType() {
    return actualType;
  }

  /**
   * Returns the CCA defined ExceptionType
   */
  public int getCCAExceptionType() {
    return gov.cca.CCAExceptionType.Nonstandard;
  }

  /**
   * Return the message associated with the exception.
   */
  public java.lang.String getNote() {
    if (note == null)
      return "No Message";
    else 
      return note;
  }

  /**
   * Set the message associated with the exception.
   */
  public void setNote(java.lang.String note_) {
    note = note_;
  }

  /**
   * The getMessage simply calls the getNote method
   */
  public String getMessage() {
    return getNote();
  }

  /**
   * Returns formatted string containing the concatenation of all 
   * tracelines.
   */
  public java.lang.String getTrace() {
    StringWriter writer = new StringWriter();
    printStackTrace(new PrintWriter(writer));
    return writer.toString();
  }

  /**
   * Adds a stringified entry/line to the stack trace.
   *
   * CURRENTLY NOT IMPLEMENTED
   */
  public void add(java.lang.String traceline) {
  }

  /**
   * Formats and adds an entry to the stack trace based on the 
   * file name, line number, and method name.
   *
   * CURRENTLY NOT IMPLEMENTED
   */
  public void add(java.lang.String filename,
		  int lineno,
		  java.lang.String methodname) {
  }
}
