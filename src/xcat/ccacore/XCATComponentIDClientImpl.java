/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.15 $ $Author: srikrish $ $Date: 2004/09/07 20:00:53 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ccacore;

import intf.ccacore.XCATServices;
import intf.ccacore.XCATComponentID;
import intf.ccacore.XCATConnectionID;
import intf.ccacore.XCATConnectionInfo;
import xcat.ports.BasicPortImpl;
import xcat.exceptions.NonstandardException;
import xcat.util.HandleResolver;

import soaprmi.util.logging.Logger;
import org.gjt.xpp.XmlNode;
import soaprmi.RemoteException;


/**
 * The implementation of XCATComponentID interface on the client side
 */
public class XCATComponentIDClientImpl implements XCATComponentID {

  protected static Logger logger = Logger.getLogger();

  protected String name;
  protected String componentHandle;

  /**
   * Constructor
   * @param name_ The name given to it by the builder service
   * @param componentHandle_ The GSH of the server side of the XCATComponentID
   */
  public XCATComponentIDClientImpl(String name_, String componentHandle_) {
    logger.finest("called");

    name = name_;
    componentHandle = componentHandle_;
  }

  //---------------------------------------------------------//
  // List of methods added by the ComponentID Interface      //
  //---------------------------------------------------------//

  /**
   * Returns the instance name provided in 
   * <code>BuilderService.createInstance()</code>
   * or in 
   * <code>AbstractFramework.getServices()</code>.
   * @throws CCAException if <code>ComponentID</code> is invalid
   */
  public java.lang.String getInstanceName() 
    throws gov.cca.CCAException {
    return name;
  }

  /**
   * Returns a framework specific serialization of the ComponentID.
   * @throws CCAException if <code>ComponentID</code> is
   * invalid.
   *
   * Currently this returns a the URL for the ComponentID (as GSH)
   */
  public java.lang.String getSerialization() 
    throws gov.cca.CCAException {
    logger.finest("called");
    return componentHandle;
  }


  /**
   * Sets the GSH for this ComponentIDClient instance
   * @param componentHandle_ the GSH for the remote ComponentID
   */
  public void setSerialization(String componentHandle_) {
    logger.finest("called");
    componentHandle = componentHandle_;
  }

  //---------------------------------------------------------//
  // List of methods added by the XCATComponentID Interface  //
  //---------------------------------------------------------//

  /**
   * Returns an array of registered provides ports
   */
  public String[] getProvidedPortNames() 
    throws gov.cca.CCAException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getProvidedPortNames();
  }

  /**
   * Returns an array of registered uses ports
   */
  public String[] getUsedPortNames()
    throws gov.cca.CCAException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getUsedPortNames();
  }

  /**
   * Returns the type of the port specified
   * @param providesPortName the name of the provides port whose type is desired
   */
  public String getProvidesPortType(String providesPortName) 
    throws gov.cca.CCAException {
    logger.finest("called for port: " + providesPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getProvidesPortType(providesPortName);
  }
  
  /**
   * Returns the type of the port specified
   * @param usesPortName the name of the uses port whose type is desired
   */
  public String getUsesPortType(String usesPortName)
    throws gov.cca.CCAException {
    logger.finest("called for port: " + usesPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getUsesPortType(usesPortName);
  }

  /**
   * Returns the GSH for the specified provides port
   * @param providesPortName the name by which the Provides Port is registered
   */
  public String getPortHandle(String providesPortName)
    throws gov.cca.CCAException {
    logger.finest("called for provides port: " + providesPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getPortHandle(providesPortName);
  }

  /**
   * Returns a GSH for the requested portName, and increments the number of users
   * for this port
   * @param providesPortName the name by which the Provides Port is registered
   */
  public String incrementUsers(String providesPortName)
    throws gov.cca.CCAException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.incrementUsers(providesPortName);
  }

  /**
   * Sets the Provides Port GSH for a uses port during a connect call
   * @param providerComponentName the instanceName of the component providing the port
   * @param providerComponentHandle the GSH for the providing component
   * @param userComponentName the instanceName of the component using the port
   * @param userComponentHandle the GSH for the using component
   * @param providingPortName the name by which the Provides Port is registered
   * @param usingPortName the name by which the Uses Port is registered
   * @param providesPortHandle GSH for the Provides Port after the connect
   */
  public void addUsesConnection(String providerComponentName,
				String providerComponentHandle,
				String userComponentName,
				String userComponentHandle,
				String providingPortName,
				String usingPortName,
				String providesPortHandle)
    throws gov.cca.CCAException {
    logger.finest("called with parameters: " +
		  " provider component = " + providerComponentName +
		  " user component = " + userComponentName +
		  " provides port name = " + providingPortName + 
		  " uses port name = " + usingPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.addUsesConnection(providerComponentName,
			     providerComponentHandle,
			     userComponentName,
			     userComponentHandle,
			     providingPortName,
			     usingPortName,
			     providesPortHandle);
  }

  /**
   * Disconnects the uses side from a connection
   * @param usingPortName the name of the uses port which is connected
   */
  public void disconnectProvider(String usingPortName)
    throws gov.cca.CCAException {
    logger.finest("called with uses port: " + usingPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.disconnectProvider(usingPortName);
  }

  /**
   * Disconnects the provides side from a connection
   * @param providingPortName the name of the provides port which is connected
   */
  public void disconnectUser(String providingPortName)
    throws gov.cca.CCAException {
    logger.finest("called with provides port: " + providingPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.disconnectUser(providingPortName);
  }

  /**
   * Returns the connection information for this uses port
   * @param usingPortName the name of the uses port which is connected
   */
  public XCATConnectionInfo getConnectionInfo(String usingPortName)
    throws gov.cca.CCAException {
    logger.finest("called with uses port: " + usingPortName);

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.getConnectionInfo(usingPortName);
  }

  /**
   * Sets the connection information for a Web service connection
   * @param wsPortName the name that a WS port has been registered as
   * @param endPointLocation the URL for the Web service to connect to
   */
  public void addWSConnection(String wsPortName,
			      String endPointLocation)
    throws gov.cca.CCAException {
    logger.finest("called with WS port: " + wsPortName);
    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.addWSConnection(wsPortName,
			   endPointLocation);
  }

  /**
   * Disconnects the Web services connection
   * @param wsPortName the name that a WS port has been registered as
   */
  public void disconnectWS(String wsPortName)
    throws gov.cca.CCAException {
    logger.finest("called with WS port: " + wsPortName);
    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.disconnectWS(wsPortName);
  }

  //---------------------------------------------------------//
  // List of methods added by the XCATPort Interface         //
  //---------------------------------------------------------//

  /**
   * Method findServiceData
   *
   * @param    queryExpression     a  XmlNode
   *
   * @return   a XmlNode
   *
   * @exception   RemoteException
   *
   */
  public XmlNode findServiceData(XmlNode queryExpression)
    throws RemoteException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.findServiceData(queryExpression);
  }
  
  /**
   * Method setServiceData
   *
   * @param    updateExpression    a  XmlNode
   *
   * @return   a XmlNode
   *
   * @exception   RemoteException
   *
   */
  public XmlNode setServiceData(XmlNode updateExpression)
    throws RemoteException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.setServiceData(updateExpression);
  }

  /**
   * Method requestTerminationAfter
   *
   * @param    terminationTime     a  XmlNode
   *
   * @return   a XmlNode
   *
   * @exception   RemoteException
   *
   */
  public XmlNode requestTerminationAfter(XmlNode terminationTime)
    throws RemoteException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.requestTerminationAfter(terminationTime);
  }

  /**
   * Method requestTermionationBefore
   *
   * @param    terminationTime     a  XmlNode
   *
   * @return   a XmlNode
   *
   * @exception   RemoteException
   *
   */
  public XmlNode requestTerminationBefore(XmlNode terminationTime)
    throws RemoteException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.requestTerminationBefore(terminationTime);
  }

  /**
   * Method destroy
   *
   * @return   a XmlNode
   *
   * @exception   RemoteException
   *
   */
  public XmlNode destroy() throws RemoteException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    return remote.destroy();
  }


  /**
   * Overloaded equals operation
   */
  public boolean equals(Object o) {
    
    try {
      // check if the instance name and GSH are the same
      if ((this.getSerialization().equals(((XCATComponentID) o).getSerialization()))
	  && (this.getInstanceName().equals(((XCATComponentID) o).getInstanceName())))
	return true;
      else
	return false;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Sets the TypeMap properties for the component. This is available
   * via the getProperties() method on the Services object.
   * @param properties the XML-ised form of the TypeMap object
   */
  public void setProperties(String properties)
    throws gov.cca.CCAException {
    logger.finest("called");

    // make a remote invocation on the server side
    XCATComponentID remote = 
      (XCATComponentID) HandleResolver.resolveHandle(componentHandle,
						     XCATComponentID.class.getName());
    remote.setProperties(properties);
  }

  //-----------------------------------------------------//
  //      Methods defined in the XCATPort Interface      //
  //-----------------------------------------------------//

  /**
   * Get the GSH for this Port
   */
  public String getGSH()
    throws gov.cca.CCAException {
    logger.finest("called");
    if (componentHandle == null)
      throw new NonstandardException("GSH for this port is not set");
    return componentHandle;
  }

  /**
   * Set the GSH for this Port
   * @param handle the GSH for this port
   */
  public void setGSH(String handle)
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + handle);
    if (componentHandle != null)
      throw new NonstandardException("GSH for this port is already set");
    componentHandle = handle;
  }
}
