/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.1 $ $Author: srikrish $ $Date: 2004/02/13 02:26:27 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ccacore;

import gov.cca.ComponentID;
import intf.ccacore.XCATConnectionInfo;

/**
 * The value object containing information about an XCATConnectionID
 */
public class XCATConnectionInfoImpl implements XCATConnectionInfo {

  // data members representing connection information
  private String providerIDHandle;
  private String userIDHandle;
  private String providerName;
  private String userName;
  private String providerPortName;
  private String userPortName;
  private String providesPortHandle;

  /**
   * Default constructor
   */
  public XCATConnectionInfoImpl() {
  }

  /**
   * Constructor
   * @param providerID_ ComponentID of the provider componenet
   * @param userID_ ComponentID of the user component
   * @param providerPortName_ name of the registered ProvidesPort
   * @param userPortName_ name of the registered UsesPort
   * @param providesPortHandle_ the GSH for the provides port
   */
  public XCATConnectionInfoImpl(ComponentID providerID_,
				ComponentID userID_,
				String providerPortName_,
				String userPortName_,
				String providesPortHandle_) 
    throws gov.cca.CCAException {
    providerIDHandle = providerID_.getSerialization();
    userIDHandle = userID_.getSerialization();
    providerName = providerID_.getInstanceName();
    userName = userID_.getInstanceName();
    providerPortName = providerPortName_;
    userPortName = userPortName_;
    providesPortHandle = providesPortHandle_;
  }

  //-------------------------------------------------//
  // Getter and Setter methods for all data members  //
  //-------------------------------------------------//

  public String getProviderIDHandle() {
    return providerIDHandle;
  }

  public String getUserIDHandle() {
    return userIDHandle;
  }

  public java.lang.String getUserPortName() {
    return userPortName;
  }

  public java.lang.String getProviderPortName() {
    return providerPortName;
  }

  public String getProviderName() {
    return providerName;
  }

  public String getUserName() {
    return userName;
  }

  public String getProvidesPortHandle() {
    return providesPortHandle;
  }

  public void setProviderIDHandle(String providerIDHandle_) {
    providerIDHandle = providerIDHandle_;
  }

  public void setUserIDHandle(String userIDHandle_) {
    userIDHandle = userIDHandle_;
  }

  public void setProviderName(String providerName_) {
    providerName = providerName_;
  }

  public void setUserName(String userName_) {
    userName = userName_;
  }

  public void setUserPortName(String userPortName_) {
    userPortName = userPortName_;
  }

  public void setProviderPortName(String providerPortName_) {
    providerPortName = providerPortName_;
  }

  public void setProvidesPortHandle(String providesPortHandle_) {
    providesPortHandle = providesPortHandle_;
  }
}
