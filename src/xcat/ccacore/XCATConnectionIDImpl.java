/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.7 $ $Author: srikrish $ $Date: 2004/02/28 01:54:02 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ccacore;

import gov.cca.ComponentID;
import intf.ccacore.XCATComponentID;
import intf.mobile.ccacore.MobileComponentID;
import intf.ccacore.XCATConnectionID;
import intf.ccacore.XCATConnectionInfo;
import xcat.util.HandleResolver;

/**
 * Implementation of the XCATConnectionID interface
 */
public class XCATConnectionIDImpl implements XCATConnectionID {
  
  // The object containing the connection information
  private XCATConnectionInfo connInfo;
  private String providerIntfName;
  private String userIntfName;

  /**
   * Constructor
   * @param providerID_ ComponentID of the provider componenet
   * @param userID_ ComponentID of the user component
   * @param providerPortName_ name of the registered ProvidesPort
   * @param userPortName_ name of the registered UsesPort
   * @param providesPortHandle_ the GSH for the provides port
   */
  public XCATConnectionIDImpl(ComponentID providerID_,
			      ComponentID userID_,
			      String providerPortName_,
			      String userPortName_,
			      String providesPortHandle_) 
    throws gov.cca.CCAException {
    connInfo = new XCATConnectionInfoImpl(providerID_,
					  userID_,
					  providerPortName_,
					  userPortName_,
					  providesPortHandle_);

    // assign the type for the interfaces
    if (MobileComponentID.class.isAssignableFrom(providerID_.getClass()))
      providerIntfName = MobileComponentID.class.getName();
    else
      providerIntfName = XCATComponentID.class.getName();
    if (MobileComponentID.class.isAssignableFrom(userID_.getClass()))
      userIntfName = MobileComponentID.class.getName();
    else
      userIntfName = XCATComponentID.class.getName();
  }

  /**
   *  Get the providing component (callee) ID.
   *  @return ComponentID of the component that has 
   *          provided the Port for this connection. 
   *  @throws CCAException if the underlying connection 
   *            is no longer valid.
   */
  public gov.cca.ComponentID getProvider() 
    throws gov.cca.CCAException {
    return (ComponentID) HandleResolver.resolveHandle(connInfo.getProviderIDHandle(),
						      providerIntfName);
  }

  /**
   *  Get the using component (caller) ID.
   *  @return ComponentID of the component that is using the provided Port.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public gov.cca.ComponentID getUser() 
    throws gov.cca.CCAException {
    return (ComponentID) HandleResolver.resolveHandle(connInfo.getUserIDHandle(),
						      userIntfName);
  }

  /**
   *  Get the port name in the providing component of this connection.
   *  @return the instance name of the provided Port.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public java.lang.String getProviderPortName() 
    throws gov.cca.CCAException {
    return connInfo.getProviderPortName();
  }

  /**
   *  Get the port name in the using component of this connection.
   *  Return the instance name of the Port registered for use in 
   *  this connection.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public java.lang.String getUserPortName() 
    throws gov.cca.CCAException {
    return connInfo.getUserPortName();
  }

  //------------------------------------------------//
  // Method added by the XCATConnectionID interface //
  //------------------------------------------------//

  /**
   * Method to get the GSH for the Provides Port for this connection
   * @return The GSH for the provides port for this connection
   */
  public String getProvidesPortHandle()
    throws gov.cca.CCAException {
    return connInfo.getProvidesPortHandle();
  }

  /**
   * Returns the connection information value object
   */
  public XCATConnectionInfo getConnectionInfo() 
    throws gov.cca.CCAException {
    return connInfo;
  }

  /**
   * Returns the FQN of the interface representing the ComponentID of the Provider
   */
  public String getProviderIntfName()
    throws gov.cca.CCAException {
    return providerIntfName;
  }

  /**
   * Returns the FQN of the interface representing the ComponentID of the User
   */
  public String getUserIntfName()
    throws gov.cca.CCAException {
    return userIntfName;
  }
}
