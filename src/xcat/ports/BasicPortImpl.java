/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.7 $ $Author: srikrish $ $Date: 2004/02/20 23:00:49 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ports;

import intf.ports.XCATPort;
import xcat.exceptions.NonstandardException;
import soaprmi.ogsi.service.grid_service.GridServiceImpl;
import soaprmi.util.logging.Logger;

/**
 * Basic implementation of an XCAT port that all port implementations
 * should extend from. This class implements (by inheritance or otherwise)
 * all methods declared in OGSI's GridServicePort
 */
public class BasicPortImpl extends GridServiceImpl 
  implements XCATPort {

  // the GSH for this port
  protected String portGSH = null;

  private static Logger logger = Logger.getLogger();

  /**
   * Initializes class and exports the object as an XSOAP service
   */
  public BasicPortImpl() throws Exception {
      super();
      logger.finest("called");
  }

  //-------------------------------------------------------------//
  //       Methods defined in the XSoapGridServiceInterface      //
  //-------------------------------------------------------------//

  /**
   * Basic destroy operation, which can be overridden by subclasses
   */
  public void destroyImpl() {
    logger.finest("called; going down in 5 seconds");

    // wait for 5 seconds, and die in a separate thread
    new Thread() {
	public void run() {
	  try {
	    Thread.sleep(5000);
	  } catch (Exception e) {
	    logger.severe(e.toString(), e);
	    System.exit(-1);
	  }
	  System.exit(0);
	} 
      }.start();
  }

  //-----------------------------------------------------//
  //      Methods defined in the XCATPort Interface      //
  //-----------------------------------------------------//

  /**
   * Get the GSH for this Port
   */
  public String getGSH()
    throws gov.cca.CCAException {
    logger.finest("called");
    if (portGSH == null)
      throw new NonstandardException("GSH for this port is not set");
    return portGSH;
  }

  /**
   * Set the GSH for this Port
   * @param handle the GSH for this port
   */
  public void setGSH(String handle)
    throws gov.cca.CCAException {
    logger.finest("called with handle: " + handle);
    if (portGSH != null)
      throw new NonstandardException("GSH for this port is already set");
    portGSH = handle;
  }
}
