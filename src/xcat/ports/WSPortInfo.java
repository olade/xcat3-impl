/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.2 $ $Author: srikrish $ $Date: 2004/09/07 23:21:55 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ports;

import soaprmi.Remote;
import soaprmi.util.logging.Logger;

import gov.cca.TypeMap;
import xcat.exceptions.NonstandardException;

/**
 * This class holds information about a registered WSPort
 */
public class WSPortInfo {

  private static Logger logger = Logger.getLogger();

  private String portName;
  private String portType;
  private TypeMap properties;
  private boolean inUse;
  private boolean unregistered;
  private String endPointLocation; // URL for remote service
  private Remote wsReference; // cached remote reference

  /**
   * Constructor
   * @param portName_ name of the registered WS port
   * @param portType_ type (namespace uri) of the registered WS port
   * @param properties_ TypeMap for the registered WS port
   */
  public WSPortInfo(String portName_, String portType_, TypeMap properties_) {
    portName = portName_;
    portType = portType_;
    properties = properties_;
    inUse = false;
    unregistered = false;
  }

  /**
   * Getter method for the portName member
   */
  public String getPortName() {
    return portName;
  }

  /**
   * Getter method for the portType member
   */
  public String getPortType() {
    return portType;
  }

  /**
   * Getter method for the properties member
   */
  public TypeMap getProperties() {
    return properties;
  }

  /**
   * Getter method for the inUse member
   */
  public boolean getInUse() {
    return inUse;
  }

  /**
   * Setter for the inUse member
   * @param inUse_ true if port is being used, false if not (released)
   */
  public void setInUse(boolean inUse_) {
    inUse = inUse_;
  }

  /**
   * Gets the reference for the remote provides port using the ConnectionID
   */
  public Remote getRemoteReference() 
    throws gov.cca.CCAException {

    // return a cached reference, if possible
    if (wsReference != null)
      return wsReference;

    // if not, create a fresh reference and return it
    String intfClassName = properties.getString("portClass", "None");
    if (intfClassName.equals("None"))
      throw new NonstandardException("Unknown interface for WS port: " +
				     portName);
    try {
      // TODO: When the WSDL will be used, all information can be inferred from it
      // Specifically, need to remove the hardcoded values for SoapStyle & SoapAction
      wsReference = soaprmi.soaprpc.SoapServices.getDefault().
	createStartpoint(endPointLocation,  // service location
			 new Class[]{Class.forName(intfClassName)}, // remote service interface
			 "", // endpoint name
			 soaprmi.soap.SoapStyle.SOAP11,
			 "" // SOAPAction
			 );
      return wsReference;
    } catch (Exception e) {
      logger.severe("Can't create a remote reference for the Web service", e);
      throw new NonstandardException("Can't create a remote reference for the Web service", 
				     e);
    }
  }

  /**
   * Unregister this uses port (needed to make sure XCATServicesImpl is thread-safe
   */
  public void unregisterPort() {
    unregistered = true;
  }

  /**
   * Return the (in)validity of this port
   */
  public boolean isUnregistered() {
    return unregistered;
  }

  /**
   * Returns the URL for the Web service endpoint
   */
  public String getEndPointLocation() {
    return endPointLocation;
  }

  /**
   * Sets the URL for the Web service endpoint
   * @param endPointLocation_ the URL for the remote Web service
   */
  public void setEndPointLocation(String endPointLocation_) {
    endPointLocation = endPointLocation_;
  }

  /**
   * Remote the connection to the remote service
   */
  public void disconnectPort() {
    endPointLocation = null;
    wsReference = null;
  }

  /**
   * Checks if port is connected to a remote service
   */
  public boolean isConnected() {
    if (endPointLocation == null)
      return false;
    else
      return true;
  }
}
