/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.9 $ $Author: srikrish $ $Date: 2004/03/15 20:50:58 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ports;

import gov.cca.Port;
import gov.cca.TypeMap;
import gov.cca.ConnectionID;
import intf.ccacore.XCATConnectionID;
import intf.ports.XCATPort;
import xcat.util.HandleResolver;
import xcat.exceptions.NonstandardException;

/**
 * This class holds information about a registered UsesPort
 */
public class UsesPortInfo {

  // constants defining status of migration of connected provides port
  private static final int NONE = 0;
  private static final int REQUESTED = 1;
  private static final int CONFIRMED = 2;
  private static final int COMPLETE = 3;

  private String portName;
  private String portType;
  private TypeMap properties;
  private boolean inUse;
  private XCATConnectionID connectionID;
  private boolean unregistered;
  private int migrationStatus;
  private XCATPort providesPortReference; // cached remote reference

  /**
   * Constructor
   * @param portName_ name of the registered uses port
   * @param portType_ type (namespace uri) of the registered provides port
   * @param properties_ TypeMap for the registered uses port
   */
  public UsesPortInfo(String portName_, String portType_, TypeMap properties_) {
    portName = portName_;
    portType = portType_;
    properties = properties_;
    inUse = false;
    migrationStatus = NONE;
    unregistered = false;
  }

  /**
   * Getter method for the portName member
   */
  public String getPortName() {
    return portName;
  }

  /**
   * Getter method for the portType member
   */
  public String getPortType() {
    return portType;
  }

  /**
   * Getter method for the properties member
   */
  public TypeMap getProperties() {
    return properties;
  }

  /**
   * Getter method for the inUse member
   */
  public boolean getInUse() {
    return inUse;
  }

  /**
   * Getter method for the connectionID member
   */
  public XCATConnectionID getConnectionID() {
    return connectionID;
  }

  /**
   * Setter for the inUse member
   * @param inUse_ true if port is being used, false if not (released)
   */
  public void setInUse(boolean inUse_) {
    inUse = inUse_;
  }

  /**
   * Setter for the connectionID member
   * @param connectionID_ the connectionID object for this connection
   */
  public void setConnectionID(XCATConnectionID connectionID_) {
    connectionID = connectionID_;
  }

  /**
   * Gets the reference for the remote provides port using the ConnectionID
   */
  public XCATPort getProvidesPortReference() 
    throws gov.cca.CCAException {

    // try to return the cached reference if possible
    if ((migrationStatus != COMPLETE) && (providesPortReference != null))
	return providesPortReference;

    // if not, resolve the handle again
    // either the migration is complete, or reference has never been created
    if ((migrationStatus == COMPLETE) || (providesPortReference == null)) {
      migrationStatus = NONE;
      
      String intfClassName = properties.getString("portClass", "None");
      if (intfClassName.equals("None"))
	throw new NonstandardException("Unknown interface for uses port: " +
				       portName);
      providesPortReference = HandleResolver.resolveHandle(connectionID.getProvidesPortHandle(),
							   intfClassName);
      return providesPortReference;
    }
    
    // can get here only if providesPortReference != null, and component is migrating
    throw new NonstandardException("Can't return a reference as component is migrating");
  }

  /**
   * Unregister this uses port (needed to make sure XCATServicesImpl is thread-safe
   */
  public void unregisterPort() {
    unregistered = true;
  }

  /**
   * Return the (in)validity of this port
   */
  public boolean isUnregistered() {
    return unregistered;
  }

  /**
   * Getter method for migrationStatus
   */
  public int getMigrationStatus() {
    return migrationStatus;
  }

  /**
   * @param migrationStatus_ the status of migration of this port
   */
  public void setMigrationStatus(int migrationStatus_) {
    migrationStatus = migrationStatus_;
  }

  /**
   * Get the status of migration
   */
  public boolean getInMigration() {
    if ((migrationStatus == NONE) || (migrationStatus == COMPLETE))
      return false;
    else 
      return true;
  }

  /**
   * Notification that the remote side requests migration
   */
  public void requestMigration() {
    migrationStatus = REQUESTED;
  }

  /**
   * Notification that the remote side will begin migrating
   */
  public void confirmMigration() {
    migrationStatus = CONFIRMED;
  }

  /**
   * Notification that the migration of the remote side is complete
   */
  public void migrationComplete() {
    migrationStatus = COMPLETE;
  }
}
