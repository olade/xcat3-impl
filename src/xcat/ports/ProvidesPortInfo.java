/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.6 $ $Author: srikrish $ $Date: 2004/03/01 20:31:42 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.ports;

import gov.cca.TypeMap;
import intf.ports.XCATPort;
import xcat.util.HandleResolver;
import xcat.exceptions.NonstandardException;

/**
 * This class holds information about a registered ProvidesPort
 */
public class ProvidesPortInfo {
  
  private String portName;
  private String portType;
  private TypeMap properties;
  private boolean inUse;
  private int numConnections;
  private String providesPortHandle;
  private boolean removed;

  /**
   * Constructor
   * @param portName_ name of the registered provides port
   * @param portType_ type (namespace uri) of the registered provides port
   * @param properties_ TypeMap for the registered provides port
   * @param providesPortHandle_ the GSH for the registered provides port
   */
  public ProvidesPortInfo(String portName_, 
			  String portType_,
			  TypeMap properties_,
			  String providesPortHandle_) {
    portName = portName_;
    portType = portType_;
    properties = properties_;
    providesPortHandle = providesPortHandle_;
    numConnections = 0;
    inUse = false;
    removed = false;
  }

  /**
   * Getter method for the portName member
   */
  public String getPortName() {
    return portName;
  }

  /**
   * Getter method for the portType member
   */
  public String getPortType() {
    return portType;
  }

  /**
   * Getter method for the properties member
   */
  public TypeMap getProperties() {
    return properties;
  }

  /**
   * Getter method for the numConnections member
   */
  public int getNumConnections() {
    return numConnections;
  }

  /**
   * Setter method for the numConnections member
   * @param numConnections_ the number of connections for this provides port
   */
  public void setNumConnections(int numConnections_) {
    numConnections = numConnections_;
  }

  /**
   * Getter method for the inUse member
   */
  public boolean getInUse() {
    return inUse;
  }

  /**
   * Getter method for the providesPortHandle member
   */
  public String getProvidesPortHandle() {
    return providesPortHandle;
  }

  /**
   * Increments the number of connections
   */
  public void incrNumConnections() {
    numConnections++;
  }

  /**
   * Decrements the number of connections
   */
  public void decrNumConnections() {
    numConnections--;
  }

  /**
   * Setter for the inUse member
   * @param inUse_ true if port is being used, false if not (released)
   */
  public void setInUse(boolean inUse_) {
    inUse = inUse_;
  }

  /**
   * Returns the reference for the Provides port stored
   */
  public XCATPort getProvidesPortReference() 
    throws gov.cca.CCAException {
    String intfClassName = properties.getString("portClass", "None");
    if (intfClassName.equals("None"))
      throw new NonstandardException("Unknown interface for provides port: " +
				     portName);
    return HandleResolver.resolveHandle(providesPortHandle,
					intfClassName);
  }

  /**
   * Remove this provides port (needed to make sure XCATServicesImpl is thread-safe
   */
  public void removePort() {
    removed = true;
  }

  /**
   * Return the (in)validity of this port
   */
  public boolean isRemoved() {
    return removed;
  }
}
