/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.7 $ $Author: srikrish $ $Date: 2004/03/05 22:35:09 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.container;

import gov.cca.ComponentID;
import gov.cca.CCAException;

import intf.ports.XCATPort;
import intf.ccacore.XCATComponentID;
import intf.container.XCATContainer;
import intf.mobile.ccacore.MobileComponent;
import xcat.mobile.ccacore.MobileServicesImpl;
import xcat.mobile.ccacore.MobileComponentIDServerImpl;
import xcat.exceptions.NonstandardException;
import xcat.util.HandleResolver;
import xcat.util.LocalServiceRegistry;

import soaprmi.ogsi.OGSI;
import soaprmi.ogsi.sde.ServiceDataManagerInterface;
import soaprmi.ogsi.sde.ServiceDataElementException;
import soaprmi.ogsi.sde.ServiceDataElement;
import soaprmi.ogsi.sde.ServiceDataElementFactory;
import soaprmi.ogsi.sde.ServiceDataElementException;
import soaprmi.ogsi.sde.ServiceDataManagerException;
import org.apache.xmlbeans.XmlQName;
import javax.xml.namespace.QName;

import soaprmi.util.logging.Logger;
import soaprmi.soaprpc.SoapServices;
import soaprmi.RemoteException;

/**
 * The Container for XCAT Components. This class is responsible
 * for creating an instance of a component, and invoking the 
 * setServices method on it
 */
public class MobileContainer implements XCATContainer {

  private static Logger logger = Logger.getLogger();

  // whether this component has been migrated from another location
  private boolean isMigrated = false;

  /**
   * Default Constructor
   */
  public MobileContainer() {
    logger.finest("called");
  }

  /**
   * Creates an instance of a Component
   *
   * @param instanceName name of the component instance
   * @param instanceHandle GSH of the ComponentID for this instance
   * @param className FQN for the class representing this component
   */
  public void createComponentInstance(String instanceName,
				      String instanceHandle,
				      String className) 
    throws CCAException {
    logger.finest("called with instanceName: " + instanceName + 
		  " and className: " + className);

    try {
      // instantiate the component specified by the className
      logger.finest("creating component instance");
      Class componentClass = Class.forName(className);
      MobileComponent component = 
	(MobileComponent) componentClass.newInstance();
      
      // create a new services object
      MobileServicesImpl services = new MobileServicesImpl(instanceName);

      // set the component for the services object
      services.setComponent(component);

      // retrieve the ComponentID for this component
      if (!(services.getComponentID() instanceof MobileComponentIDServerImpl))
	throw new NonstandardException("ComponentID not an instance of MobileComponentIDServerImpl");
      MobileComponentIDServerImpl cid = 
	(MobileComponentIDServerImpl) services.getComponentID();

      // set the GSH for this ComponentID
      cid.setGSH(instanceHandle);

      // Add a reference to this ComponentID into the Handle Resolver
      HandleResolver.addReference(instanceHandle, cid);

      // if this component has been migrated, return here
      if (isMigrated) {
	logger.finest("Component is migrated, setServices will not be invoked");
	return;
      }

      // invoke the setServices method on the component
      try {
	component.setServices(services);
      } catch (Exception e) {
	logger.severe("Can't invoke setServices on component", e);
	throw new NonstandardException("Can't invoke setServices on component", e);
      }
      
      // add some SDEs to the Component
      logger.finest("adding default Service Data Elements");
      ServiceDataManagerInterface sdm =
	cid.getServiceDataManager();
      ServiceDataElementFactory sdeFactory = 
	ServiceDataElementFactory.getDefault();
      
      // an SDE for the name of the Component
      XmlQName serviceQName = XmlQName.Factory.newInstance();
      serviceQName.setQNameValue(new QName("http://www.extreme.indiana.edu/xcat/ccacore/componentID",
					   instanceName));
      ServiceDataElement serviceNameSDE =
	sdeFactory.newServiceDataElement(new QName(OGSI.OGSI_NS,
						   "instanceName"),
					 serviceQName);
      sdm.addServiceData(serviceNameSDE);

      // add SDEs for GSH's of each of the ports
      String[] portNames = services.getProvidedPortNames();
      for (int i = 0; i < portNames.length; i++) {
	String portHandle = services.getPortHandle(portNames[i]);
	String handleXMLString =
	  "<tns:providesPortHandle " +
	  "xmlns:tns=\"http://www.extreme.indiana.edu/xcat/ccacore/componentID\" " +
	  "name=\"" + portNames[i] + "\">" +
	  portHandle +
	  "</tns:providesPortHandle>";
	ServiceDataElement handleSDE =
	  sdeFactory.newServiceDataElement(handleXMLString);
	sdm.addServiceData(handleSDE);
      }

      String handleXMLString =
	"<tns:componentHandle " +
	"xmlns:tns=\"http://www.extreme.indiana.edu/xcat/ccacore/componentID\" " +
	"name=\"" + instanceName + "\">" +
	instanceHandle +
	"</tns:componentHandle>";
      ServiceDataElement handleSDE =
	sdeFactory.newServiceDataElement(handleXMLString);
      sdm.addServiceData(handleSDE);

    } catch (ClassNotFoundException cnfe) {
      logger.severe("Can't find class " + className + " for component", cnfe);
      throw new NonstandardException("Can't find class " + className + 
				     " for component", cnfe);
    } catch(InstantiationException ie) {
      logger.severe("Can't instantiate class " + className, ie);
      throw new NonstandardException("Can't instantiate class " + className,
				     ie);
    } catch (IllegalAccessException iae) {
      logger.severe("Can't instantiate class " + className, iae);
      throw new NonstandardException("Can't instantiate class " + className,
				     iae);
    } catch (ServiceDataManagerException sdme) {
      logger.severe("Can't create ServiceDataManager for ComponentID", sdme);
      throw new NonstandardException("Can't create ServiceDataManager for ComponentID",
				     sdme);
    } catch (ServiceDataElementException sdee) {
      logger.severe("Can't add service data to ComponentID", sdee);
      throw new NonstandardException("Can't add service data to ComponentID",
				     sdee);
    }
  }

  /**
   * set the flag that signifies that this component is migrated from another location
   */
  public void setIsMigrated() {
    logger.finest("called");
    isMigrated = true;
  }
}
