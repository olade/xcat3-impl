/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.1 $ $Author: srikrish $ $Date: 2004/03/17 01:10:15 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.mobile.ccacore;

/**
 * This class holds information about status of uses ports (inUse or released)
 */
public class MigrationStatus {

  // outstanding number of unreleased ports
  private int numUnreleasedPorts;

  // status of freezeComponent request
  private boolean freezeRequested;

  // if a component is frozen or not
  private boolean isFrozen;

  /**
   * Default constructor
   */
  public MigrationStatus() {
    numUnreleasedPorts = 0;
    freezeRequested = false;
    isFrozen = false;
  }

  /**
   * Set the number of unreleased ports
   * @param numUnreleasedPorts_ number of ports in use
   */
  public void setNumUnreleasedPorts(int numUnreleasedPorts_) {
    numUnreleasedPorts = numUnreleasedPorts_;
  }

  /**
   * Get the number of unreleased ports
   */  
  public int getNumUnreleasedPorts() {
    return numUnreleasedPorts;
  }

  /**
   * Increment the number of unreleased ports
   */  
  public void incrNumUnreleasedPorts() {
    numUnreleasedPorts++;
  }

  /**
   * Decrement the number of unreleased ports
   */  
  public void decrNumUnreleasedPorts() {
    numUnreleasedPorts--;
  }
  
  /**
   * Get the status of a freezeComponent request
   */
  public boolean getFreezeRequested() {
    return freezeRequested;
  }

  /**
   * Set the status of a freezeComponent request
   */
  public void setFreezeRequested(boolean freezeRequested_) {
    freezeRequested = freezeRequested_;
  }

  /**
   * Get the status of the isFrozen flag
   */
  public boolean getIsFrozen() {
    return isFrozen;
  }

  /**
   * Set the status of the isFrozen flag
   */
  public void setIsFrozen(boolean isFrozen_) {
    isFrozen = isFrozen_;
  }
}
