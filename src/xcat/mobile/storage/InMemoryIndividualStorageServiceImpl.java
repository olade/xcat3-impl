/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.1 $ $Author: srikrish $ $Date: 2004/09/09 06:55:32 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.mobile.storage;

import intf.mobile.storage.MasterStorageService;
import intf.mobile.storage.IndividualStorageService;

import xcat.ports.BasicPortImpl;
import xcat.exceptions.NonstandardException;

import soaprmi.util.logging.Logger;
import soaprmi.server.RemoteRef;
import soaprmi.server.UnicastRemoteObject;
import soaprmi.soaprpc.SoapServices;

import java.util.Hashtable;
import java.util.Random;

import simple.ParamParser;

/**
 * Implementation for the service that stores a list of Individual Storage
 * Services and balances the loads between them
 */

public class InMemoryIndividualStorageServiceImpl extends BasicPortImpl
  implements IndividualStorageService {

  private static Logger logger = Logger.getLogger();

  // the Hashtable where the states are stored
  Hashtable storageMap;

  // random number generator
  Random random;

  /**
   * Constructor
   * @param serviceName the name to use for the service
   * @param portNum the port number to use, 0 for random available port
   */
  public InMemoryIndividualStorageServiceImpl(String serviceName, 
					      int portNum)
    throws Exception {
    logger.finest("called with serviceName: " + serviceName);

    // initialize the members
    storageMap = new Hashtable();
    random = new Random();

    // export this object as a Grid service
    UnicastRemoteObject.exportObject(this,
				     portNum,
				     serviceName,
				     new Class[]{IndividualStorageService.class});
  }

  /**
   * Store given state into stable storage, and return a 
   * unique storageID that can be used to access the state
   * at a later time
   * @param state the state to be stored into stable storage
   * @return unique storageID
   */
  public synchronized String storeState(String state)
    throws gov.cca.CCAException {
    logger.finest("called");

    // generate storageID
    String storageID = 
      ((RemoteRef) this).getSoapRMIPort().getEndpoint().getLocation() + 
      "/" + Math.abs(random.nextInt());
    logger.finest("generated storageID: " + storageID);

    // simulate a write to a database
    // Assume 10 MB/s, 1 char = 2B
    int sleepTime = (state.length() * 2) / 10000; 
    logger.info("sleeping for : " + sleepTime + " ms");
    try {
      Thread.sleep(sleepTime);
    } catch (Exception e) {
      logger.severe("error while sleeping/writing to DB", e);
      throw new NonstandardException("Error while sleeping/writing to DB", e);
    }

    // put the state into the map, indexed by the storageID
    storageMap.put(storageID, state);

    // return the storageID
    return storageID;
  }

  /**
   * Retrieved state from stable storage, using the unique
   * storageID
   * @param storageID the unique ID needed to access stored state
   */
  public synchronized String retrieveState(String storageID)
    throws gov.cca.CCAException {
    logger.finest("called for storageID: " + storageID);

    // make sure the state exists in this ISS
    if (!storageMap.containsKey(storageID))
      throw new NonstandardException("ISS doesn't contain state for storageID: " +
				     storageID);

    // return the state
    String state = (String) storageMap.get(storageID);
    // simulate a read from a database
    // Assume 100 MB/s, 1 char = 2B
    int sleepTime = (state.length() * 2) / 100000; 
    logger.info("sleeping for : " + sleepTime + " ms");
    try {
      Thread.sleep(sleepTime);
    } catch (Exception e) {
      logger.severe("error while sleeping/reading from DB", e);
      throw new NonstandardException("Error while sleeping/reading from DB", e);
    }

    return state;
  }

  /**
   * Delete state from stable storage, using the unique
   * storageID
   * @param storageID the unique ID needed to access stored state
   */
  public synchronized void deleteState(String storageID)
    throws gov.cca.CCAException {
    logger.finest("called for storageID: " + storageID);

    // make sure the state exists in this ISS
    if (!storageMap.containsKey(storageID))
      throw new NonstandardException("ISS doesn't contain state for storageID: " +
				     storageID);

    // delete the entry
    storageMap.remove(storageID);
  }

  // following code copied and modified from GSX
  // http://www.extreme.indiana.edu/xgws/GSX
  private static final int SERVICE_PORT = 0;
  private static final int SERVICE_NAME = 1;
  private static final int MASTER_URL = 2;
  private static final String USAGE =
    "java xcat.mobile.storage.InMemoryIndividualStorageServiceImpl " +
    "[-name service_name] [-port port_number] -master master_storage_url";

  private static String[] parseArgs(String[] args) 
    throws Exception {
    
    // set arg names expected
    String[] argNames = new String[3];
    argNames[SERVICE_PORT] = "-port";
    argNames[SERVICE_NAME] = "-name";
    argNames[MASTER_URL]   = "-master";
    
    // set defaults
    String[] defaultValues = new String[3];
    defaultValues[SERVICE_PORT] = "0";
    defaultValues[SERVICE_NAME] = "individual-storage-service";
    defaultValues[MASTER_URL] = null;

    return ParamParser.parse(args, argNames, defaultValues, USAGE);
  }

  public static void main(String[] args) throws Exception {
    
    // parse arguments
    String[] myArgs = parseArgs(args);
    String serviceName = myArgs[SERVICE_NAME];
    int portNum = Integer.parseInt(myArgs[SERVICE_PORT]);
    String masterURL = myArgs[MASTER_URL];
    if (masterURL == null) {
      System.out.println(USAGE);
      System.exit(1);
    }
      
    // instantiate the MSS
    IndividualStorageService iss = new InMemoryIndividualStorageServiceImpl(serviceName,
									    portNum);
    logger.finest("IndividualStorageService instantiated");

    MasterStorageService mss = (MasterStorageService) SoapServices.getDefault().
      createStartpoint(masterURL,  // service location
		       new Class[]{MasterStorageService.class}, // remote service interface
		       "", // endpoint name
		       soaprmi.soap.SoapStyle.SOAP11,
		       "" // SOAPAction
		       );
    String issLocation = ((RemoteRef) mss).getSoapRMIPort().getEndpoint().getLocation();
    mss.registerIndividualStorageService(issLocation);
    logger.finest("Registered location of service with the MasterStorageService");

    logger.finest("IndividualStorageService up and available at: " + issLocation);
  }
}
