/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.1 $ $Author: srikrish $ $Date: 2004/09/09 06:55:32 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.mobile.storage;

import intf.mobile.storage.MasterStorageService;

import xcat.ports.BasicPortImpl;
import xcat.exceptions.NonstandardException;

import soaprmi.util.logging.Logger;
import soaprmi.server.RemoteRef;
import soaprmi.server.UnicastRemoteObject;

import java.util.Vector;

import simple.ParamParser;

/**
 * Implementation for the service that stores a list of Individual 
 * Storage Services and balances the loads between them
 */

public class InMemoryMasterStorageServiceImpl extends BasicPortImpl 
  implements MasterStorageService {

  private static Logger logger = Logger.getLogger();

  // List of Individual Storage Services Registered
  Vector individualStorageServices;

  // the index of the service to return
  int index;

  /**
   * Constructor
   * @param serviceName the name to use for the service
   * @param portNum the port number to use, 0 for random available port
   */
  public InMemoryMasterStorageServiceImpl(String serviceName, 
					  int portNum)
    throws Exception {
    logger.finest("called with serviceName: " + serviceName);

    // initialize the vector of IndividualStorageServices
    individualStorageServices = new Vector();
    index = 0;

    // export this object as a Grid service
    UnicastRemoteObject.exportObject(this,
				     portNum,
				     serviceName,
				     new Class[]{MasterStorageService.class});
  }

  /**
   * Get the URL for the best available Individual Storage
   * Service
   */
  public synchronized String getIndividualStorageServiceLocation()
    throws gov.cca.CCAException {
    logger.finest("called");

    // make sure there is an individual storage service available
    int size = individualStorageServices.size();
    if (size == 0)
      throw new NonstandardException("No IndividualStorageServices have been registered");

    // retrieve the next available ISS location
    String location = (String) individualStorageServices.get(index);
    logger.finest("retrieved location: " + location);

    // update the index
    index = (index + 1) % size;

    // return the location
    return location;
  }

  /**
   * Register the URL for an Individual Storage Service
   * @param location the URL of an Individual Storage Service 
   * to register
   */
  public synchronized void registerIndividualStorageService(String location)
    throws gov.cca.CCAException {
    logger.finest("called with location: " + location);

    // simply append the location of the ISS to the vector
    individualStorageServices.add(location);
  }

  // following code copied and modified from GSX
  // http://www.extreme.indiana.edu/xgws/GSX
  private static final int SERVICE_PORT = 0;
  private static final int SERVICE_NAME = 1;
  private static final String USAGE =
    "java xcat.mobile.storage.InMemoryMasterStorageServiceImpl " +
    "[-name service_name] [-port port_number]";

  private static String[] parseArgs(String[] args) 
    throws Exception {
    
    // set arg names expected
    String[] argNames = new String[2];
    argNames[SERVICE_PORT] = "-port";
    argNames[SERVICE_NAME] = "-name";
    
    // set defaults
    String[] defaultValues = new String[2];
    defaultValues[SERVICE_PORT] = "0";
    defaultValues[SERVICE_NAME] = "master-storage-service";
    
    return ParamParser.parse(args, argNames, defaultValues, USAGE);
  }

  public static void main(String[] args) throws Exception {
    
    // parse arguments
    String[] myArgs = parseArgs(args);
    String serviceName = myArgs[SERVICE_NAME];
    int portNum = Integer.parseInt(myArgs[SERVICE_PORT]);

    // instantiate the MSS
    MasterStorageService mss = new InMemoryMasterStorageServiceImpl(serviceName,
								    portNum);
    logger.finest("MasterStorageService up and available at: " +
		  ((RemoteRef) mss).getSoapRMIPort().getEndpoint().getLocation());
  }
}
