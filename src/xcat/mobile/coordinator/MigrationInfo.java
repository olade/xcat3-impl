/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.5 $ $Author: srikrish $ $Date: 2004/09/09 06:55:32 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package xcat.mobile.coordinator;

import intf.mobile.coordinator.AppCoordinatorCallback;

/**
 * This class holds information about the migration status
 */
public class MigrationInfo {

  // number of uses ports connected to this component
  private int numUsers;

  // number of users who have responsed with migration approval
  private int approvedUsers;

  // a flag to signify if the component has been successfully frozen
  private boolean isFrozen;

  // a flag with the status of migration request
  private int migrationStatus;

  /**
   * Constructor
   * @param numUsers_ the total number of uses ports connected to this component
   */
  public MigrationInfo(int numUsers_) {
    numUsers = numUsers_;
    approvedUsers = 0;
    isFrozen = false;
    migrationStatus = AppCoordinatorCallback.READY;
  }

  /**
   * increment the number of users who have approved the migration
   */
  public void incrApprovedUsers() {
    approvedUsers++;
  }

  /**
   * reset the approved users (done when migration is complete)
   */
  public void resetApprovedUsers() {
    approvedUsers = 0;
  }

  /**
   * return the number of users who have approved the migration
   */
  public int getApprovedUsers() {
    return approvedUsers;
  }

  /**
   * return the number of uses ports connected to this component
   */
  public int getNumUsers() {
    return numUsers;
  }
  
  /**
   * Getter for the isFrozen variable
   */
  public boolean getIsFrozen() {
    return isFrozen;
  }

  /**
   * Setter for the isFrozen variable
   * @param isFrozen_ true if notification has been received that component is frozen
   */
  public void setIsFrozen(boolean isFrozen_) {
    isFrozen = isFrozen_;
  }

  /**
   * Gets the status of migration
   */
  public int getMigrationStatus() {
    return migrationStatus;
  }

  /**
   * Sets the migration status
   * @param migrationStatus_ one of AppCoordinatorCallback.READY, 
   *                                AppCoordinatorCallback.FROZEN,
   *                                AppCoordinatorCallback.EXCEPTION
   */
  public void setMigrationStatus(int migrationStatus_) {
    migrationStatus = migrationStatus_;
  }
}
